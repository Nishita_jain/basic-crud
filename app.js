const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
const db = require('./db/index');
const config = require('./config');
app.use(express.json());
const addEmployee = require('./handler/addEmployee');
const getEmployee = require('./handler/getEmployee');
const updateEmployee = require('./handler/updateEmployee');
const deleteEmployee = require('./handler/deleteEmployee');
const getMaxSalary = require('./handler/getMaxSalary');
const getEmployeeById = require('./handler/getEmployeeId');

db.connect()
    .then(() => {
        app.listen(config.port, console.log(`server is started on port ${config.port}`));

    });

app.get('/employees', getEmployee.get);

app.get('/employee/:employeeId', getEmployeeById.getById);

app.post('/employee', addEmployee.add);

app.put('/employee/:employeeId', updateEmployee.update)

app.delete('/employee/:employeeId', deleteEmployee.delete);

app.get('/employees/max/salary', getMaxSalary.getHighestSalary)

module.exports = app;





