const mongoose = require('mongoose');

const EmployeeSchema = new mongoose.Schema({
    employeeId : {
        type : Number,
        required : true ,
        unique : true
    },
    name : {
        type : String,
        required : true
    },
    age : {
        type : Number,
        required : true
    },
    designation : {
        type : String
    },
    skills : {
        type : Array
    },
    salary : {
        type : Number,
        required : true
    }
})

const Employee = mongoose.model('Employees' , EmployeeSchema  , 'Employees');
module.exports = { Employee};