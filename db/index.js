const mongoose = require('mongoose');
const config = require('../config');

function connect() {
    return new Promise((resolve, reject) => {
        mongoose.connect(config.srv, { useUnifiedTopology: true, useNewUrlParser: true })
            .then((res, err) => {
                if (err) {
                    return reject(err);
                } else {
                    console.log('=> connected to db successfully !!');
                    resolve();
                }
            })
    })
}

function close() {
    return mongoose.disconnect();
}


module.exports = { connect, close };