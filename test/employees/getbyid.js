const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const conn = require('../../db/index');


describe('GET /employee/:employeeId', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('should fetch employee  detail by id ', (done) => {
        request(app).get('/employee/3')
            .then((res) => {
                const body = res.body;
                expect(body.length).to.equal(1);
                expect(res.statusCode).to.equal(200);
                expect(Object.keys(body[0])).to.include.members(['age', 'employeeId', 'name', 'salary']);
                done();
            })
            .catch((err) => done(err))
    })

    it('should not fetch employee if employee id is invalid ', (done) => {
        request(app).get('/employee/3')
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.equal('Employee Id Not exist')
                done();
            })
            .catch((err) => done(err))
    })
})