const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const conn = require('../../db/index');


describe('PUT /employee/:employeeId', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('update employee woking', (done) => {
        request(app).put('/employee/5')
            .send({
                "name": "emp5",
            })
            .then((res) => {
                const body = res.body;
                expect(body).to.have.property('ok', 1)
                done();
            })
            .catch((err) => done(err))
    })
})