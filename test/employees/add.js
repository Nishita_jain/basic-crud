const expect = require('chai').expect;
const app = require('../../app');
const request = require('supertest');
const conn = require('../../db/index');


describe('POST /employee', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('should create the employee record', (done) => {
        request(app).post('/employee')
            .send({
                "name": "emp9",
                "employeeId": 9,
                "age": 24,
                "designation": "tester",
                "salary": 37000
            })
            .then((res) => {
                console.log(res)
                const body = res.body;
                expect(res.statusCode).to.equal(200);
                expect(body).to.contain.property('name');
                expect(body).to.contain.property('age');
                expect(body).to.contain.property('employeeId');
                expect(body).to.contain.property('salary');
                done();
            })
            .catch((err) => done(err))
    })


    it('should not create the employee record if employee id  already exist ', (done) => {
        request(app).post('/employee')
            .send({
                "name": "emp5",
                "employeeId": 7,
                "age": 24,
                "designation": "tester",
                "salary": 37000
            })
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res).to.contain.property('text', 'Employee Id already exist');
                done();
            })
            .catch((err) => done(err))
    })


    it('should not create the employee record if employee id  field is not given', (done) => {
        request(app).post('/employee')
            .send({
                "name": "emp5",
                "age": 24,
                "designation": "tester",
                "salary": 37000
            })
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res).to.contain.property('text', 'employeeId Key is required !!');
                done();
            })
            .catch((err) => done(err))
    })

    it('should not create the employee record if name field is not given', (done) => {
        request(app).post('/employee')
            .send({
                "employeeId": 5,
                "age": 24,
                "designation": "tester",
                "salary": 37000
            })
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res).to.contain.property('text', 'name Key is required !!');
                done();
            })
            .catch((err) => done(err))
    })

    it('should not create the employee record if age  field is not given', (done) => {
        request(app).post('/employee')
            .send({
                "name": "emp5",
                "employeeId": 24,
                "designation": "tester",
                "salary": 37000
            })
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res).to.contain.property('text', 'age Key is required !!');
                done();
            })
            .catch((err) => done(err))
    })

    it('should not create the employee record if salary  field is not given', (done) => {
        request(app).post('/employee')
            .send({
                "name": "emp5",
                "age": 24,
                "designation": "tester",
                "employeeId": 5
            })
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res).to.contain.property('text', 'salary Key is required !!');
                done();
            })
            .catch((err) => done(err))
    })
})