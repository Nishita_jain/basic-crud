const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const conn = require('../../db/index');


describe.only('GET /employees', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('Should fetch the employee record', (done) => {
        request(app).get('/employees')
            .then((res) => {
                const body = res.body;
                expect(res.statusCode).to.equal(200);
                expect(body).to.have.lengthOf.above(0);
                done();
            })
            .catch((err) => done(err))
    })
})