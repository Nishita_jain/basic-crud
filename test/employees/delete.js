const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const conn = require('../../db/index');


describe('DELETE /employee/:employeeId', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('should delete employee record', (done) => {
        request(app).delete('/employee/5')
            .then((res) => {
                expect(res.statusCode).to.equal(200);
                expect(res.text).to.equal('Sucessfully Deleted !!')
                done();
            })
            .catch((err) => done(err))
    })

    it('should not delete the employee record if employee id not exist ', (done) => {
        request(app).delete('/employee/5')
            .then((res) => {
                expect(res.statusCode).to.equal(400);
                expect(res.text).to.equal('Employee Id Not exist')
                done();
            })
            .catch((err) => done(err))
    })
})