const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const conn = require('../../db/index');


describe('GET /employees/max/salary', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('GET employee woking', (done) => {
        request(app).get('/employees/max/salary')
            .then((res) => {
                const text = res.text;
                expect(text).to.be.a("string");
                done();
            })
            .catch((err) => done(err))
    })
})