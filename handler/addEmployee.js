
const Employee = require('../db/models/employee').Employee;
const RequiredFieldMapping = require('../config').RequiredFieldMapping;

const validateBody = (body) => {
    for (let x of RequiredFieldMapping) {
        if (!Object.keys(body).includes(x)) {
            return x;
        }
    };
    return 'validated';
}

const validateEmployeeId = (id) => {
    return Employee.findOne({ employeeId: parseInt(id) });
}

exports.add = (req, res) => {
    console.log("body :: ", req.body);
    let bodyValidationResponse = validateBody(req.body);
    if (bodyValidationResponse == 'validated') {
        validateEmployeeId(req.body.employeeId)
            .then((data) => {
                if (data) {
                    res.status(400).send('Employee Id already exist');
                } else {
                    const employee = new Employee(req.body);
                    employee.save(employee)
                        .then((result) => {
                            console.log(result)
                            res.status(201).send(result)

                        })
                }
            })
            .catch((err) =>
                res.status(400).send(err));
    } else {
        res.status(400).send(`${bodyValidationResponse} Key is required !!`)
    }


}
