
const Employee = require('../db/models/employee').Employee;

exports.get = (req, res) => {
    Employee.find()
        .then((employees) =>
            res.status(200).send(employees))
        .catch((err) =>
            res.status(400).send(err));
}
