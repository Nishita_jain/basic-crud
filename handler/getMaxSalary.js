
const Employee = require('../db/models/employee').Employee;

exports.getHighestSalary = (req, res) => {
    Employee.find().sort({ salary: -1 })
        .then((employees) =>
            res.status(200).send(`${employees[0].name} has the highest salry`))
        .catch((err) =>
            res.status(400).send(err));
}

