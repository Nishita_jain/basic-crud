const Employee = require('../db/models/employee').Employee;
const FieldsMapping = ['name', 'age', 'designation', 'skills', 'salary']

const validateBody = function (body) {
    console.log('Inside validate body');
    for (let key of Object.keys(body)) {
        if (!(FieldsMapping.includes(key))) {
            return false;
        }
    }
    return true;
};

exports.update = async (req, res) => {
    console.log(req.params.employeeId);
    console.log("body :: ", req.body);
    if (validateBody(req.body)) {
        Employee.updateOne({ employeeId: parseInt(req.params.employeeId) }, { $set: req.body })
            .then((employee) =>
                res.status(200).send(employee))
            .catch((err) =>
                res.status(400).send(err));
    } else {
        res.status(400).send('Employee Id can not update ')
    }
}

