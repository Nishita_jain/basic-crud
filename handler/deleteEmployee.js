
const Employee = require('../db/models/employee').Employee;


const validateEmployeeId = (id) => {
    return Employee.findOne({ employeeId: parseInt(id) });
}

exports.delete = (req, res) => {
    console.log(req.params.employeeId);
    validateEmployeeId(req.params.employeeId)
        .then((data) => {
            console.log(data)
            if (data) {
                Employee.deleteOne({ employeeId: parseInt(req.params.employeeId) })
                    .then((response) => {
                        if (response.deletedCount == 1) {
                            res.status(200).send('Sucessfully Deleted !!')
                        }
                    })
            } else {
                res.status(400).send('Employee Id Not exist')
            }
        })
        .catch((err) =>
            res.status(400).send(err));

}
