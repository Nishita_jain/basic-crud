
const Employee = require('../db/models/employee').Employee;

exports.getById = (req, res) => {
    console.log('employeeId::', req.params.employeeId);
    Employee.find({ employeeId: parseInt(req.params.employeeId) })
        .then((employee) => {
            if (employee.length) {
                res.status(200).send(employee)
            } else {
                res.status(400).send('Employee Id Not exist')
            }
        })
        .catch((err) =>
            res.status(400).send(err));
}
