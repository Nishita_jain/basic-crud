module.exports = {
    srv: "mongodb+srv://test:test@cluster0-usk23.mongodb.net/test?retryWrites=true&w=majority",
    port: 8000,
    dbName: "testdb",
    employee_Collection: "employee",
    RequiredFieldMapping: ["name", "employeeId", "age", "salary"]
}